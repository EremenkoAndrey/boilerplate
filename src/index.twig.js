const template = require('./index.twig');
const logo = require('./images/webpack.png');

const image = {
    src: logo
};

module.exports = template({ image });
