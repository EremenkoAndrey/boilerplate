import './example-component.css';
import './example-component.less';

class Test {
    constructor(num) {
        this.test = num;
    }

    init() {
        // eslint-disable-next-line
        console.log(this.test += 1);
    }
}

export default Test;

