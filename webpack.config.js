const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const merge = require('webpack-merge');
const devConfig = require('./config/builder/webpack.dev.config.js');
const prodConfig = require('./config/builder/webpack.prod.config.js');

const config = process.env.NODE_ENV !== 'production' ? devConfig : prodConfig;

module.exports = merge(config, {
    entry: {
        index: './src/',
        vendors: './src/vendors'
    },
    output: {
        filename: '[name].js',
        path: `${__dirname}/dist`
    },
    plugins: [
        new CleanWebpackPlugin(['dist']),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: './src/index.twig.js'
        }),
        new ExtractTextPlugin('styles/styles.css')
    ],

    module: {
        rules: [
            {
                test: /\.twig$/,
                use: [
                    'twig-loader'
                ],
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                exclude: /node_modules/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            },
            {
                test: /\.less$/,
                exclude: /node_modules/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        'css-loader',
                        {
                            loader: 'postcss-loader',
                            options: {
                                config: {
                                    path: './config/builder/postcss.config.js'
                                }
                            }
                        },
                        'less-loader']
                })
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[path][name].[ext]',
                            context: 'src/'
                        }
                    }
                ]
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[path][name].[ext]',
                            context: 'src/'
                        }
                    }
                ]
            }
        ]
    }
});

