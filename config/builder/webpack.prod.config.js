const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
    output: {
        publicPath: ''
    },
    plugins: [
        new UglifyJSPlugin({
            sourceMap: true
        })
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [
                    'eslint-loader'
                ]
            },
        ]
    }
};
