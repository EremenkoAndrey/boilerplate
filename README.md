# README #

Заготовка для верстки приложения с использованием шаблонизатора Twig.js

Скачивание:
```git clone https://bitbucket.org/EremenkoAndrey/boilerplate/ <newname>```

Вместо newname подставить название нового проекта

### Запуск в режиме разработки ###

```npm start```

### Запуск сервера expess в режиме разработки ###

```npm run server```

### Сборка проекта ###

```npm run build```

